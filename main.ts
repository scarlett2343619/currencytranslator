console.log("Main.js working");

interface CurrencyData {
  [key: string]: {
    code: string;
    value: number;
  };
}

const populate = async (value: number, currency: string) => {
  let myStr = "";
  const url = `https://api.currencyapi.com/v3/latest?apikey=cur_live_7UStkUqQNBmahSoy8K635tE3Sjr5fK1UVPmVloZ2&base_currency=${currency}`;
  const response = await fetch(url);
  const rJson: { data: CurrencyData } = await response.json();
  document.querySelector(".output")!.style.display = "block";

  for (const key of Object.keys(rJson.data)) {
    myStr += `
      <tr>
        <td>${key}</td>
        <td>${rJson.data[key].code}</td>
        <td>${Math.round(rJson.data[key].value * value)}</td>
      </tr>
    `;
  }
  const tableBody = document.querySelector("tbody");
  tableBody!.innerHTML = myStr;
};

const btn = document.querySelector(".btn");
btn!.addEventListener("click", (e: MouseEvent) => {
  e.preventDefault();
  const value = parseInt((document.querySelector("input[name='quantity']") as HTMLInputElement).value);
  const currency = (document.querySelector("select[name='currency']") as HTMLSelectElement).value;
  populate(value, currency);
});